package ru.stackprod.robotics;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.stackprod.robotics.POJO.Stop;
import ru.stackprod.robotics.POJO.StopsList;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    ListView stopsListView;
    StopsAdapter adapter;
    ArrayList<Stop> stopObjs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        stopsListView = (ListView) findViewById(R.id.stops_listview);
        InfoGetter infoGetter = new InfoGetter();
        infoGetter.getStops(new Callback<StopsList>() {
            @Override
            public void onResponse(Call<StopsList> call, Response<StopsList> response) {
                stopObjs = response.body().getData();
                adapter = new StopsAdapter(MainActivity.this, R.layout.item_stop, stopObjs);
                stopsListView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<StopsList> call, Throwable t) {
                Toast.makeText(MainActivity.this, "При загрузке данных произошла ошибка", Toast.LENGTH_SHORT).show();
            }
        });


        stopsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(MainActivity.this, RoutesActivity.class);
                intent.putExtra("name", stopObjs.get(position).getName());
                intent.putExtra("id", stopObjs.get(position).getId());
                startActivity(intent);
            }
        });
    }
}
