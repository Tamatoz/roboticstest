package ru.stackprod.robotics.POJO;

/**
 * Created by Руслан on 14.04.2016.
 */
public class Stop {
    public double getLat() {
        return lat;
    }

    public double getLon() {
        return lon;
    }

    public String getName() {
        return name;
    }

    public String[] getRouteTypes() {
        return routeTypes;
    }

    public String getId() {
        return id;
    }

    private double lat, lon;
    private String name;
    private String[] routeTypes;
    private String id;
}
