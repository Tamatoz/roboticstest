package ru.stackprod.robotics;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.Switch;
import android.widget.TextView;
import ru.stackprod.robotics.POJO.Route;
import ru.stackprod.robotics.POJO.Stop;

import java.util.ArrayList;

/**
 * Created by Руслан on 14.04.2016.
 */
public class RoutesAdapter extends ArrayAdapter {
    ArrayList<Route> stops;
    int resID;
    Context mContext;
    public RoutesAdapter(Context context, int resource, ArrayList<Route> stops) {
        super(context, resource, stops);
        this.stops = stops;
        this.resID = resource;
        this.mContext = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        RoutesViewHolder holder;
        if(convertView == null) {
            convertView = View.inflate(mContext, resID, null);
            holder = new RoutesViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.number);
            holder.icon = (ImageView) convertView.findViewById(R.id.icon);
            holder.route = (TextView) convertView.findViewById(R.id.route);
            convertView.setTag(holder);
        } else {
            holder = (RoutesViewHolder) convertView.getTag();
        }
        holder.name.setText(stops.get(position).getShortName());
        holder.route.setText(stops.get(position).getName());
        switch(stops.get(position).getType()){
            case "trol":
                holder.icon.setImageResource(R.drawable.trol_icon);
                break;
            case "tram":
                holder.icon.setImageResource(R.drawable.tram_icon);
                break;
            case "m":
            case "bus":
                holder.icon.setImageResource(R.drawable.m_icon);
        }
        return convertView;
    }

    private static class RoutesViewHolder{
        TextView name, route;
        ImageView icon;
    }
}
