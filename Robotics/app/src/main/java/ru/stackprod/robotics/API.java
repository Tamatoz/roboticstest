package ru.stackprod.robotics;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;
import ru.stackprod.robotics.POJO.RoutesList;
import ru.stackprod.robotics.POJO.Stop;
import ru.stackprod.robotics.POJO.StopsList;

import java.util.ArrayList;

/**
 * Created by Руслан on 14.04.2016.
 */
public interface API {
    @GET("/api/v2/db/stops")
    Call<StopsList> getStops();
    @GET("api/v2/db/routes")
    Call<RoutesList> getRoutes(@Query("stopId") String id);
}
