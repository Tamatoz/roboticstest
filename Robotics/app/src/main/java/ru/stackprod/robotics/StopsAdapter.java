package ru.stackprod.robotics;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import ru.stackprod.robotics.POJO.Stop;

import java.util.ArrayList;

/**
 * Created by Руслан on 14.04.2016.
 */
public class StopsAdapter extends ArrayAdapter {
    ArrayList<Stop> stops;
    int resID;
    Context mContext;
    public StopsAdapter(Context context, int resource, ArrayList<Stop> stops) {
        super(context, resource, stops);
        this.stops = stops;
        this.resID = resource;
        this.mContext = context;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        StopsViewHolder holder;
        if(convertView == null) {
            convertView = View.inflate(mContext, resID, null);
            holder = new StopsViewHolder();
            holder.name = (TextView) convertView.findViewById(R.id.stop_name);
            convertView.setTag(holder);
        } else {
            holder = (StopsViewHolder) convertView.getTag();
        }
        holder.name.setText(stops.get(position).getName());
        return convertView;
    }

    private static class StopsViewHolder{
        TextView name;
    }
}
