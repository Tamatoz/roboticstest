package ru.stackprod.robotics.POJO;

/**
 * Created by Руслан on 14.04.2016.
 */
public class Route {
    public String getName() {
        return name;
    }

    public String getType() {
        return type;
    }

    private String name;
    private String type;

    public String getShortName() {
        return shortName;
    }

    private String shortName;

}
