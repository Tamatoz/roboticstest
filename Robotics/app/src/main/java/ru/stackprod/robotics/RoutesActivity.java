package ru.stackprod.robotics;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import ru.stackprod.robotics.POJO.RoutesList;
import ru.stackprod.robotics.POJO.Stop;
import ru.stackprod.robotics.POJO.StopsList;

import java.util.ArrayList;

/**
 * Created by Руслан on 14.04.2016.
 */
public class RoutesActivity extends AppCompatActivity {
    ListView routesListView;
    RoutesAdapter adapter;

    TextView stopName;
    String name = "", id = "";
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_routes);
        routesListView = (ListView) findViewById(R.id.routes_listview);
        stopName = (TextView) findViewById(R.id.stop_name);
        name = getIntent().getStringExtra("name");
        id = getIntent().getStringExtra("id");
        stopName.setText(name);
        InfoGetter infoGetter = new InfoGetter();

        infoGetter.getRoutes(new Callback<RoutesList>() {
            @Override
            public void onResponse(Call<RoutesList> call, Response<RoutesList> response) {
                adapter = new RoutesAdapter(RoutesActivity.this, R.layout.item_routes, response.body().getData());
                routesListView.setAdapter(adapter);
            }

            @Override
            public void onFailure(Call<RoutesList> call, Throwable t) {
                Toast.makeText(RoutesActivity.this, "При загрузке данных произошла ошибка", Toast.LENGTH_SHORT).show();
            }
        }, id);
    }
}
