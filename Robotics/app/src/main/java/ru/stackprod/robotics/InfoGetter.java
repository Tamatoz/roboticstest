package ru.stackprod.robotics;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import ru.stackprod.robotics.POJO.RoutesList;
import ru.stackprod.robotics.POJO.Stop;
import ru.stackprod.robotics.POJO.StopsList;

import java.util.ArrayList;

/**
 * Created by Руслан on 14.04.2016.
 */
public class InfoGetter {
    private Retrofit retrofit = new Retrofit.Builder()
            .baseUrl("http://mobileapps.krd.ru:9000")
            .addConverterFactory(GsonConverterFactory.create())
            .build();
    private API api = retrofit.create(API.class);

    public void getStops(Callback<StopsList> callback){
        Call<StopsList> call = api.getStops();
        call.enqueue(callback);
    }

    public void getRoutes(Callback<RoutesList> callback, String id){
        Call<RoutesList> call = api.getRoutes(id);
        call.enqueue(callback);
    }
}
